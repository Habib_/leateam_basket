import { Route, Switch } from 'react-router';
import Shop from './Pages/Shop/Shop';
import Other from './Pages/Other/Other';
import Products from './Pages/Products/Products';

function App() {
  return (
    <Switch>
      <Route path="/shop" exact component={Shop} />
      <Route path="/shop/:cat/:id" exact component={Products} />

      <Route component={Other} />
    </Switch>
  );
}

export default App;
