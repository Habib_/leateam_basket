import React from 'react';
import PropTypes from "prop-types";

import leftArrow from '../../Static/Images/Icons/left.svg'

import styles from './Category.module.scss';

const Category = props => {
  return (
    <div className={styles.CategoryBox}>
      <div
        data-name={props.text}
        data-id={props.id}
        onClick={(e) => props.onClick(e.currentTarget.dataset.name, e.currentTarget.dataset.id)}
        className={styles.CategoryWrapper}
      >
        <img className={styles.catImage} src={props.image} alt={props.text} />
        
        <p>  {props.text} <span><img className={styles.arrow} src={leftArrow} alt='left arrow' /></span> </p>
      </div>
    </div>
  )
}

Category.propType = {
  image: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
}

export default React.memo(Category)
