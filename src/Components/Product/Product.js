import React, { useEffect, useState } from 'react';

import { NumberSeperator } from '../../Static/utils/NumberSeperator';
import { BaseURL } from '../../Store/Api/Request';

import Basket from '../../Static/Images/Icons/buy.svg'

import styles from './Product.module.scss';

const Product = props => {

  const [ShowProductAdded, setShowProductAdded] = useState(false)

  useEffect(() => {
    if (ShowProductAdded) {
      setTimeout(() => {
        setShowProductAdded(false)
      }, 1500);
    }
  }, [ShowProductAdded])

  const handleAddingProductToBasket = () => {
    props.onClick(props.product)

    // show message for adding product
    setShowProductAdded(true)
  }

  return (
    <div className={styles.ProductWrapper}>
      <div className={styles.productBox}>
        <div className={styles.productsImageWrapper}>
          <img src={BaseURL + props.product.small_pic} alt={props.product.title} />
          {
            props.product.isNew ?
              <span className={styles.isNew}>جدید</span>
              :
              null
          }
        </div>
        <div className={styles.productTitle}>
          <h5>{props.product.title}</h5>
        </div>
        <p className={styles.weight}>{props.product.weight}ml</p>
        <div className={styles.inlineItems}>
          <span className={styles.buy}>
            {
              ShowProductAdded ?
              <p> محصول، اضافه شد </p>
              :
                <img onClick={handleAddingProductToBasket} src={Basket} alt='basket' />
            }
          </span>
          <div className={styles.money}>
            {NumberSeperator(props.product.price.final_price)}
            <p className={styles.unit}>تومان</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default React.memo(Product)
