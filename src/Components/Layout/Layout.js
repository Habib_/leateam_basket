import React from "react";

import PropTypes from "prop-types";

import Footer from "../Footer/Footer";
import Header from "../Header/Header";

import styles from "./Layout.module.scss";

const Layout = props => {
  return (
    <div className={styles.LayoutWrapper}>
      <Header />
      <div className={styles.children}>
        {props.children}
      </div>
      <Footer />
    </div>
  );
};

export default React.memo(
  Layout,
  (prevProps, nextProps) =>
    prevProps.children === nextProps.children
);
