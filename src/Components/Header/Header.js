import React, { Fragment, useEffect, useRef, useState } from 'react';
import { NavLink } from 'react-router-dom';

import LiaTeamLogo from "../../Static/Images/Icons/liaHeaderLogo.svg";
import UserProfile from "../../Static/Images/Icons/Userpic.svg";
import DownArrow from "../../Static/Images/Icons/down.svg";
import Basket from "../../Static/Images/Icons/cart.svg";

import styles from './Header.module.scss';
import BasketList from '../BasketList/BasketList';
import { connect } from 'react-redux';

const Header = props => {

  const [ShowBasket, setShowBasket] = useState(false)

  function useOutsideAlerter(ref) {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setShowBasket(false)
        }
      }
      // Bind the event listener
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  }

  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef);

  return (
    <div className={styles.headerWrapper}>
      <Fragment>
        <div className={styles.topLine}>
          <div className={styles.menuWrapper}>
            <div className={styles.logoWrapper}>
              <img src={LiaTeamLogo} alt="liateam" className={styles.liaLogo} />
            </div>

            <NavLink
              to="/"
              activeClassName={styles.isActive}
              className={styles.menuItem}
              exact
            > خانه </NavLink>
            <NavLink
              to="/shop"
              activeClassName={styles.isActive}
              className={styles.menuItem}
            > فروشگاه </NavLink>
            <NavLink
              to="/blog"
              activeClassName={styles.isActive}
              className={styles.menuItem}
              exact
            > وبلاگ </NavLink>
            <NavLink
              to="/about-us"
              activeClassName={styles.isActive}
              className={styles.menuItem}
              exact
            > درباره ما </NavLink>
            <NavLink
              to="/contact-us"
              activeClassName={styles.isActive}
              className={styles.menuItem}
              exact
            > تماس با ما </NavLink>
          </div>

          <div className={styles.userInfo}>
            <div className={styles.userNameWrapper}>
              <p className={styles.userName}>رضا پورجباری عزیز</p>
            </div>
            <div className={styles.imageWrapper}>
              <img
                className={styles.userImage}
                src={UserProfile}
                alt="user info"
              />
              <img
                className={styles.downArrow}
                src={DownArrow}
                alt="down arrow"
              />
            </div>
          </div>
        </div>

        {/* second menu */}
        <div className={styles.secondLine}>
          <div className={styles.filterMenu}>
            <NavLink
              to="/shop/مراقبت پوست/10"
              className={styles.filtermenuItem}
              activeClassName={styles.filterMenuIsActive}
            >مراقبت پوست
            <div className={styles.active}></div>
            </NavLink>

            <NavLink
              to="/shop/مراقبت مو/11"
              className={styles.filtermenuItem}
              activeClassName={styles.filterMenuIsActive}
            >مراقبت مو
            <div className={styles.active}></div>
            </NavLink>

            <NavLink
              to="/shop/مراقبت بدن/90"
              className={styles.filtermenuItem}
              activeClassName={styles.filterMenuIsActive}
            >مراقبت بدن
            <div className={styles.active}></div>
            </NavLink>

            <NavLink
              to="/shop/آرایشی/91"
              className={styles.filtermenuItem}
              activeClassName={styles.filterMenuIsActive}
            >آرایشی
            <div className={styles.active}></div>
            </NavLink>

            <NavLink
              to="/shop/پرفروش ترین/1"
              className={styles.filtermenuItem}
              activeClassName={styles.filterMenuIsActive}
            >پرفروش ترین
            <div className={styles.active}></div>
            </NavLink>

            <NavLink
              to="/shop/جدید ترین/1"
              className={styles.filtermenuItem}
              activeClassName={styles.filterMenuIsActive}
            >جدید ترین
            <div className={styles.active}></div>
            </NavLink>
          </div>

          <div onClick={() => setShowBasket(true)} className={styles.basketWrapper}>
            <img
              src={Basket}
              alt="my basket list"
            />
            {/* <span className={styles.itemInside}> {props.CountOfProducts} </span> */}
            {ShowBasket && (
              <Fragment>
                <div className={styles.arrowBox}></div>

                <div
                  className={styles.basketList}
                  ref={wrapperRef}
                >
                <BasketList />
                </div>
              </Fragment>
            )}
          </div>

        </div>
      </Fragment>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    // CountOfProducts: state.CountOfProducts,
  };
};

const mapDispatchToProps = () => {
  return {
    //
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header)
