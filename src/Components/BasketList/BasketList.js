import React, { Fragment, useState } from 'react';
import { connect } from 'react-redux';
import { BaseURL } from '../../Store/Api/Request';

import remove from '../../Static/Images/Icons/Cross.svg';

import styles from './BasketList.module.scss';
import { NumberSeperator } from '../../Static/utils/NumberSeperator';
import { removeItemFromBasketAction } from '../../Store/Actions/Action';

const BasketList = props => {

  const [Basket, setBasket] = useState(props.BasketState)

  const handleRemoveItem = (id, cost) => {
    let currentBasket = props.BasketState;
    let currentCost = props.ProductsPrice;
  
    let finalCost = currentCost - cost;
  
    let index = currentBasket.findIndex(item => item.id === id)
  
    if(currentBasket[index].count === 1){
      currentBasket.splice(index, 1)
    } else {
      currentBasket[index].count = currentBasket[index].count - 1;
    }

    props.removeItemFromBasket({basket: currentBasket, cost: finalCost, count: currentBasket.length})
    setBasket(currentBasket)
  }

  return (
    <div className={styles.basketWrapper}>
      {
        Basket.length === 0 ?
          <p className={styles.noProduct}> محصولی در سبد وجود ندارد </p>
          :
          <Fragment>
            {
              Basket.map(product => (
                <div key={product.id} className={styles.itemInBasket}>
                  <img className={styles.productImage} src={BaseURL + product.image} />
                  <div className={styles.productParams}>
                    <span className={styles.title}>{product.title}</span>
                    <span className={styles.cost}>{NumberSeperator(product.cost)}تومان</span>
                    <div className={styles.countWrapper}>  <p>{product.count} عدد</p> </div>
                    <span onClick={() => handleRemoveItem(product.id, product.cost)} className={styles.removeIcon}> <img src={remove} alt="remove item" /> </span>
                  </div>
                </div>
              ))
            }
            <div className={[styles.itemInBasket, styles.payPart].join(" ")}>
              <p className={styles.payTitle}> مبلغ قابل پرداخت : </p>
              <p className={styles.pay}> {NumberSeperator(props.ProductsPrice)} تومان </p>
            </div>
            <div className={styles.itemInBasket}>
              <button className={styles.button}> ثبت سفارش </button>
            </div>
          </Fragment>
      }
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    BasketState: state.BasketState,
    ProductsPrice: state.ProductsPrice,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    removeItemFromBasket: (data) => dispatch(removeItemFromBasketAction(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(React.memo(BasketList));
