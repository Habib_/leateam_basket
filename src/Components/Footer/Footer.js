import React from "react";

import Logo from "../../Static/Images/Icons/leaLogo.svg";
import Twitter from "../../Static/Images/Icons/twitter.svg";
import Instagram from "../../Static/Images/Icons/instagram.svg";
import YouTube from "../../Static/Images/Icons/youTube.svg";
import FaceBook from "../../Static/Images/Icons/facebook.svg";
import Email from "../../Static/Images/Icons/email.svg";
import Tel from "../../Static/Images/Icons/tel.svg";

import styles from "./Footer.module.scss";

const Footer = () => {

  return (
    <div className={styles.footerWrapper}>

      <div className={styles.inlineItems}>
        <div className={styles.infoWrapper}>
          <div className={styles.info}>
            <p>مرکز پشتیبانی بازاریابان</p>
          </div>
          <div className={styles.Vline}></div>
          <div className={styles.info}>
            <p>021 88 88 88 88</p>
            <img src={Tel} alt='call lia team' />
          </div>
          <div className={styles.Vline}></div>
          <div className={styles.info}>
            <p>sellersupport@liateam.com</p>
            <img src={Email} alt='email to lia team' />
          </div>
        </div>

        <div className={styles.logoWrapper}>
          <p>Good Time Good News</p>
          <h4>Lia Virtual Office</h4>
          <div className={styles.logoBox}>
            <img src={Logo} alt='lia team logo' />
          </div>
        </div>
      </div>


      <div className={styles.Hline}>
        <div className={styles.Hline}></div>
      </div>


      <div className={styles.inlineItems}>
        <h6>هفت روز هفته ، ۲۴ ساعت شبانه‌روز پاسخگوی شما هستیم</h6>

        <div className={styles.socialWrapper}>
          <div className={styles.social}>
            <img src={YouTube} alt="liaTeam YouTube channel" />
          </div>
          <div className={styles.social}>
            <img src={Twitter} alt="liaTeam Twitter channel" />
          </div>
          <div className={styles.socialF}>
            <img src={FaceBook} alt="liaTeam FaceBook channel" />
          </div>
          <div className={styles.social}>
            <img src={Instagram} alt="liaTeam Instagram channel" />
          </div>
        </div>

        <div className={styles.copy}>
          <p>
            © تمام حقوق این وب سایت متعلق به شرکت آرمان تدبیر اطلس 1398-1397 می باشد
          </p>
        </div>

      </div>

    </div>
  );
};

export default React.memo(Footer);