import React from 'react';
import PropTypes from 'prop-types';

import styles from './Loader.module.scss';

const Loader = props => {
  return (
    <div className={styles.inlineItems}>
      <div className={[styles.loader, props.color == 'white' ? styles.whiteColor : null].join(" ")}>
        <div></div>
        <div></div>
        <div></div>
      </div>
      <p className={[styles.loader_text, props.color == 'white' ? styles.whiteColor : null].join(" ")}> {props.loadingText} </p>
    </div>
  )
}

Loader.propTypes = {
  loadingText: PropTypes.string,
}

Loader.defaultProps = {
  loadingText: "Loading",
}

export default React.memo(Loader)
