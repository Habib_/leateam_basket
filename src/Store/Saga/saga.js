import { takeLatest, call, put } from "redux-saga/effects";
import * as actionTypes from "../Actions/ActionType";
import * as actions from "../Actions/Action";

import {
  getCategoriesListApi,
  getProductsListApi,
} from '../Api/ProductsApi'


export function* getCategoriesListSaga(action) {
  try {
    const response = yield call(getCategoriesListApi);
    yield put(actions.getCategoriesListSuccessAction(response.data));
  } catch (error) {
    yield put(actions.getCategoriesListFailAction(error));
  }
}
export function* getProductsListSaga(action) {
  try {
    const response = yield call(getProductsListApi, action.data);
    yield put(actions.getProductsListSuccessAction(response.data));
  } catch (error) {
    yield put(actions.getProductsListFailAction(error));
  }
}


export function* Saga() {
  yield takeLatest(actionTypes.GET_CATEGORIES_LIST, getCategoriesListSaga);
  yield takeLatest(actionTypes.GET_PRODUCTS_LIST, getProductsListSaga);
}
