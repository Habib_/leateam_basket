import { request } from "./Request";

export function getCategoriesListApi() {
  return request().get("/api/rest/v1/get_categories");
}

export function getProductsListApi(data) {
  return request().get(`/api/rest/v1/get_product?categories=${data.id}?page=${data.page}`);
}