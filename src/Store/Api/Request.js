import axios from "axios";

export const BaseURL = "https://shopapi.liateam.com";

export function request() {
  const request = axios.create({
    baseURL: BaseURL,
    timeout: 8000,
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
    },
  });

  request.interceptors.request.use(
    function (config) {
      return config;
    },
    function (error) {
      return Promise.reject(error);
    }
  );
  return request;
}
