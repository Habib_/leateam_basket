import * as actionType from "./ActionType";


export const getCategoriesListAction = () => {
  return { type: actionType.GET_CATEGORIES_LIST };
};
export const getCategoriesListSuccessAction = (response) => {
  return { type: actionType.GET_CATEGORIES_LIST_SUCCESS, response };
};
export const getCategoriesListFailAction = (error) => {
  return { type: actionType.GET_CATEGORIES_LIST_FAIL, error };
};


export const getProductsListAction = (data) => {
  return { type: actionType.GET_PRODUCTS_LIST, data };
};
export const getProductsListSuccessAction = (response) => {
  return { type: actionType.GET_PRODUCTS_LIST_SUCCESS, response };
};
export const getProductsListFailAction = (error) => {
  return { type: actionType.GET_PRODUCTS_LIST_FAIL, error };
};



export const addItemToBasketAction = (data) => {
  return { type: actionType.ADD_ITEM_TO_BASKET, data };
};

export const removeItemFromBasketAction = (data) => {
  return { type: actionType.REMOVE_ITEM_FROM_BASKET, data };
};