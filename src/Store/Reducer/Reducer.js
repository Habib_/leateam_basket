import * as actionType from "../Actions/ActionType";
import { updateObject } from "../../Static/utils/UpdateObject";

const initialState = {
  getCategoriesListState: {
    request: null,
    response: null,
    error: null,
  },
  getProductsListState: {
    request: null,
    response: null,
    error: null,
  },
  BasketState: [],
  ProductsPrice: 0,
  CountOfProducts: 0,
}


const removeItemFromBasket = (state, action) => {
  return (
    updateObject(state, { CountOfProducts: action.data.count }) &&
    updateObject(state, { BasketState: action.data.basket }) &&
    updateObject(state, { ProductsPrice: action.data.cost }) 
    );
}

const addItemToBasket = (state, action) => {
  return (
    updateObject(state, { CountOfProducts: action.data.count }) &&
    updateObject(state, { BasketState: action.data.basket }) &&
    updateObject(state, { ProductsPrice: action.data.cost })
    );
};


const getCategoriesList = (state, action) => {
  const updateValue = {
    request: action,
    response: null,
    error: null,
  };
  return updateObject(state, { getCategoriesListState: updateValue });
};

const getCategoriesListSuccess = (state, action) => {
  const updateValue = {
    request: null,
    response: action.response,
    error: null,
  };
  return updateObject(state, { getCategoriesListState: updateValue });
};

const getCategoriesListFail = (state, action) => {
  const updateValue = {
    request: null,
    response: null,
    error: action.error,
  };
  return updateObject(state, { getCategoriesListState: updateValue });
};




const getProductsList = (state, action) => {
  const updateValue = {
    request: action,
    response: null,
    error: null,
  };
  return updateObject(state, { getProductsListState: updateValue });
};

const getProductsListSuccess = (state, action) => {
  const updateValue = {
    request: null,
    response: action.response,
    error: null,
  };
  return updateObject(state, { getProductsListState: updateValue });
};

const getProductsListFail = (state, action) => {
  const updateValue = {
    request: null,
    response: null,
    error: action.error,
  };
  return updateObject(state, { getProductsListState: updateValue });
};




const Reducer = (state = initialState, action) => {
  switch (action.type) {

    case actionType.GET_CATEGORIES_LIST:
      return getCategoriesList(state, action);
    case actionType.GET_CATEGORIES_LIST_SUCCESS:
      return getCategoriesListSuccess(state, action);
    case actionType.GET_CATEGORIES_LIST_FAIL:
      return getCategoriesListFail(state, action);


    case actionType.GET_PRODUCTS_LIST:
      return getProductsList(state, action);
    case actionType.GET_PRODUCTS_LIST_SUCCESS:
      return getProductsListSuccess(state, action);
    case actionType.GET_PRODUCTS_LIST_FAIL:
      return getProductsListFail(state, action);
      
      case actionType.ADD_ITEM_TO_BASKET:
        return addItemToBasket(state, action);
      
      case actionType.REMOVE_ITEM_FROM_BASKET:
        return removeItemFromBasket(state, action);

    default:
      return state;
  }
};

export default Reducer;