import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import Layout from '../../Components/Layout/Layout';
import Loader from '../../Components/Loader/Loader';
import Product from '../../Components/Product/Product';
import { addItemToBasketAction, getProductsListAction } from '../../Store/Actions/Action';

import styles from './Products.module.scss';

const Products = props => {

  useEffect(() => {
    const id = props.match.params.id;

    let data = {
      id: parseInt(id),
      page: 1,
    }

    props.getProductsList(data)

  }, [props.match.params])

  const handleAddItemToBasket = (product) => {

    let currentBasket = props.BasketState;
    let currentCost = props.ProductsPrice;

    let newItem = {
      id: product.id,
      title: product.title,
      image: product.small_pic,
      cost: product.price.final_price,
      count: 1,
    }

    let finalCost = currentCost + product.price.final_price

    // if basket was empty
    if (currentBasket.length === 0) {
      currentBasket.push(newItem)
    } else {
      const index = currentBasket.findIndex(item => item.id === product.id);

      // if product exists in basket
      if (index === -1) {
        currentBasket.push(newItem);
      } else {
        currentBasket[index].count = currentBasket[index].count + 1;
      }
    }

    props.addItemToBasket(
      {
        basket: currentBasket,
        cost: finalCost,
        count: currentBasket.length,
      }
    )
  }

  return (
    <Layout>
      {
        props.getProductsListState.request !== null ?
          <div className={styles.loaderWrapper}>
            <Loader />
          </div>
          :
          props.getProductsListState.response !== null ?
            <div className={styles.productsWrapper}>
              {
                props.getProductsListState.response.list.length > 0 ?
                  props.getProductsListState.response.list.map(item => (
                    <Product
                      key={item.id}
                      product={item}
                      onClick={(product) => handleAddItemToBasket(product)}
                    />
                  ))
                  :
                  <div> موردی وجود ندارد </div>
              }
            </div>
            :
            props.getProductsListState.error !== null ?
              <div className={styles.hasError}> مشکلی در سرور به وجود آمده لطفا مجددا تلاش کنید </div>
              :
              null
      }
    </Layout>
  )
}

const mapStateToProps = (state) => {
  return {
    getProductsListState: state.getProductsListState,
    BasketState: state.BasketState,
    ProductsPrice: state.ProductsPrice,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getProductsList: (data) => dispatch(getProductsListAction(data)),
    addItemToBasket: (data) => dispatch(addItemToBasketAction(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);
