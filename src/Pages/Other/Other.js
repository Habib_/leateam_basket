import React from 'react';
import Layout from '../../Components/Layout/Layout';

import styles from './Other.module.scss';

const Other = props => {

  const handleGoToShop = () => {
    props.history.push('/shop')
  }

  return (
    <Layout>
      <div className={styles.pageWrapper}>
        <h2>از فروشگاه ما دیدن فرمایید</h2>
        <button onClick={handleGoToShop} className={styles.button}> دیدن محصولات </button>
      </div>
    </Layout>
  )
}

export default Other
