import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import Category from '../../Components/Category/Category';
import Layout from '../../Components/Layout/Layout';
import Loader from '../../Components/Loader/Loader';
import { BaseURL } from '../../Store/Api/Request'

import { getCategoriesListAction } from '../../Store/Actions/Action';

import styles from './Shop.module.scss';

const Shop = props => {

  useEffect(() => {
    props.getCategoriesList();
  }, [])

  // when choose category
  const handleClickedOnCat = (val, id) => {
    props.history.push(`/shop/${val}/${id}`)
  }

  return (
    <Layout>
      <h2 className={styles.pageTitle}>دسته بندی</h2>
      {
        props.getCategoriesListState.request !== null ?
          <div className={styles.loaderWrapper}>
            <Loader />
          </div>
          :
          props.getCategoriesListState.response !== null ?
            <div className={styles.CatWrapper}>
              {
                props.getCategoriesListState.response.map(cat => (
                  <Category key={cat.id} id={cat.id} text={cat.name} image={BaseURL + cat.image} onClick={(val, id) => handleClickedOnCat(val, id)} />
                ))
              }
            </div>
            :
            props.getCategoriesListState.error !== null ?
              <div className={styles.hasError}> مشکلی در سرور به وجود آمده لطفا مجددا تلاش کنید </div>
              :
              null
      }
    </Layout>
  )
}

const mapStateToProps = (state) => {
  return {
    getCategoriesListState: state.getCategoriesListState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCategoriesList: () => dispatch(getCategoriesListAction()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Shop)
