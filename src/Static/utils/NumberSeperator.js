export const NumberSeperator = (num) => {
  if (num.toString().length <= 3) {
    return num;
  } else if (num.toString().length <= 6) {
    return num.toString().slice(-6, -3) + "," + num.toString().slice(-3);
  } else if (num.toString().length <= 9) {
    return (
      num.toString().slice(-9, -6) +
      "," +
      num.toString().slice(-6, -3) +
      "," +
      num.toString().slice(-3)
    );
  } else if (num.toString().length <= 12) {
    return (
      num.toString().slice(-12, -9) +
      "," +
      num.toString().slice(-9, -6) +
      "," +
      num.toString().slice(-6, -3) +
      "," +
      num.toString().slice(-3)
    );
  }
};